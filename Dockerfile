FROM openjdk:11
VOLUME /tmp
EXPOSE 8082
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} cm-tool-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","cm-tool-0.0.1-SNAPSHOT.jar"]
