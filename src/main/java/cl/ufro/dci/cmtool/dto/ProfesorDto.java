package cl.ufro.dci.cmtool.dto;

import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.entity.Rol;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.*;

public class ProfesorDto implements UserDetails {

    @NotNull(message = "El nombre no puede estar en blanco")
    private String nombreProfesor;

    @Email(message = "Debe ingresar un email")
    @NotNull(message = "El email no puede estar en blanco")
    private String correoProfesor;

    @NotNull(message = "Debe ingresar una contraseña")
    private String contraseniaProfesor;

    private Collection<? extends GrantedAuthority> authorities;
    private Set<String> roles = new HashSet<>();


    public ProfesorDto(String nombreProfesor, @Email String correoProfesor, String contraseniaProfesor, Collection<? extends GrantedAuthority> authorities) {
        this.nombreProfesor = nombreProfesor;
        this.correoProfesor = correoProfesor;
        this.contraseniaProfesor = contraseniaProfesor;
        this.authorities = authorities;
    }

    public static ProfesorDto build(Profesor profesor){
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Rol rol : profesor.getRolProfesor()) {
            authorities.add(new SimpleGrantedAuthority(rol.getRolNombre().name()));
        }
        return new ProfesorDto(profesor.getNombreProfesor(), profesor.getCorreoProfesor(), profesor.getContraseniaProfesor(), authorities);
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }

    public String getCorreoProfesor() {
        return correoProfesor;
    }

    public void setCorreoProfesor(String correoProfesor) {
        this.correoProfesor = correoProfesor;
    }

    public String getContraseniaProfesor() {
        return contraseniaProfesor;
    }

    public void setContraseniaProfesor(String contraseniaProfesor) {
        this.contraseniaProfesor = contraseniaProfesor;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return contraseniaProfesor;
    }

    @Override
    public String getUsername() {
        return correoProfesor;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
