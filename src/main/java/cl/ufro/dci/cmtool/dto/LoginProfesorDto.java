package cl.ufro.dci.cmtool.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Clase de tipo Dto utilizada para la transmision de datos
 * Esta clase contiene los atributos necesarios para la autentificacion
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
public class LoginProfesorDto implements Serializable {

    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String password;

    public LoginProfesorDto(@NotBlank @Email String email, @NotBlank String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
