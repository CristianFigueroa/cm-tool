package cl.ufro.dci.cmtool.dto;

import javax.validation.constraints.NotNull;

public class ModuloDto {

    @NotNull(message = "Debe ingresar un código para el módulo")
    private String codigoModulo;

    public ModuloDto() {
    }

    public String getCodigoModulo() {
        return codigoModulo;
    }

    public void setCodigoModulo(String codigoModulo) {
        this.codigoModulo = codigoModulo;
    }

}
