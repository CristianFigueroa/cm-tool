package cl.ufro.dci.cmtool.dto;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Clase de tipo Dto utilizada para la transmision de datos
 * Esta clase contiene los atributos de un Token
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
public class JwtDto {

    private String token;
    private String bearer = "Bearer";
    private String emailUsuario;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtDto(String token, String emailUsuario, Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.emailUsuario = emailUsuario;
        this.authorities = authorities;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    public String getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
