package cl.ufro.dci.cmtool.dto;

import java.util.HashSet;
import java.util.Set;

public class RubricaDto {

    private Set<IndicadorDto> indicadores = new HashSet<>();

    public RubricaDto() {
    }

    public RubricaDto(Set<IndicadorDto> indicadores) {
        this.indicadores = indicadores;
    }

    public Set<IndicadorDto> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(Set<IndicadorDto> indicadores) {
        this.indicadores = indicadores;
    }

}
