package cl.ufro.dci.cmtool.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class SemestreDto {

    @NotNull
    @Min(value = 2020, message = "El año del semestre debe ser mayor a 2020")
    @Max(value = 3000, message = "El año del semestre debe ser menor a 3000")
    private int anioSemestre;

    @NotNull
    @Min(value = 1, message = "El número del semestre debe ser 1 ó 2")
    @Max(value = 2, message = "El número del semestre debe ser 1 ó 2")
    private int numeroSemestre;

    public int getAnioSemestre() {
        return anioSemestre;
    }

    public void setAnioSemestre(int anioSemestre) {
        this.anioSemestre = anioSemestre;
    }

    public int getNumeroSemestre() {
        return numeroSemestre;
    }

    public void setNumeroSemestre(int numeroSemestre) {
        this.numeroSemestre = numeroSemestre;
    }
}
