package cl.ufro.dci.cmtool.dto;

public class IndicadorDto {

    private boolean evaluado;

    private int puntajeIndicador;

    private int idTipoIndicador;

    public IndicadorDto() {
    }

    public IndicadorDto(boolean evaluado, int puntajeIndicador, int idTipoIndicador) {
        this.evaluado = evaluado;
        this.puntajeIndicador = puntajeIndicador;
        this.idTipoIndicador = idTipoIndicador;
    }

    public boolean isEvaluado() {
        return evaluado;
    }

    public void setEvaluado(boolean evaluado) {
        this.evaluado = evaluado;
    }

    public int getPuntajeIndicador() {
        return puntajeIndicador;
    }

    public void setPuntajeIndicador(int puntajeIndicador) {
        this.puntajeIndicador = puntajeIndicador;
    }

    public int getIdTipoIndicador() {
        return idTipoIndicador;
    }

    public void setIdTipoIndicador(int idTipoIndicador) {
        this.idTipoIndicador = idTipoIndicador;
    }
}
