package cl.ufro.dci.cmtool.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AlumnoDto {

    @NotNull(message = "La matrícula no puede estar en blanco")
    private String matricula;
    @NotNull(message = "El nombre no puede estar en blanco")
    private String nombre;
    @Min(value = 1000, message = "El código de carrera no puede estar en blanco")
    @Max(value = 9999, message = "El código de carrera no puede estar en blanco")
    private int codigoCarrera;

    public AlumnoDto() {
    }

    public AlumnoDto(String matricula, String nombre, int carrera) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.codigoCarrera = carrera;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(int codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }
}
