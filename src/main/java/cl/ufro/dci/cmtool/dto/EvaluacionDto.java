package cl.ufro.dci.cmtool.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class EvaluacionDto {

    @NotNull
    private Date fechaEvaluacion;

    private int numeroEvaluacion;

    private String descripcion;

    private int numeroPlantilla;

    private int idEvaluacion;

    public EvaluacionDto() {
    }

    public EvaluacionDto(Date fechaEvaluacion, int numeroEvaluacion, String descripcion, int numeroPlantilla) {
        this.fechaEvaluacion = fechaEvaluacion;
        this.numeroEvaluacion = numeroEvaluacion;
        this.descripcion = descripcion;
        this.numeroPlantilla = numeroPlantilla;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public Integer getNumeroEvaluacion() {
        return numeroEvaluacion;
    }

    public void setNumeroEvaluacion(int numeroEvaluacion) {
        this.numeroEvaluacion = numeroEvaluacion;
    }

    public void setNumeroEvaluacion(Integer numeroEvaluacion) {
        this.numeroEvaluacion = numeroEvaluacion;
    }

    public int getIdEvaluacion() {
        return idEvaluacion;
    }

    public void setIdEvaluacion(int idEvaluacion) {
        this.idEvaluacion = idEvaluacion;
    }

    public int getNumeroPlantilla() {
        return numeroPlantilla;
    }

    public void setNumeroPlantilla(int numeroPlantilla) {
        this.numeroPlantilla = numeroPlantilla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
