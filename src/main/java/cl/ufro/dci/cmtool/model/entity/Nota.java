package cl.ufro.dci.cmtool.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Notas")
public class Nota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNota;

    @NotNull
    private int nota;

    @ManyToOne
    @JoinColumn(name = "idAlumno_n", nullable = false, updatable = false)
    private Alumno alumno;

    @ManyToOne
    @JoinColumn(name = "idEvaluacion_n", nullable = false, updatable = false)
    private Evaluacion evaluacion;

    @OneToOne
    @JoinColumn(name = "idRubrica_n", updatable = false, nullable = false)
    private Rubrica rubrica;

    public Nota() {
    }

    public Nota(@NotNull int nota) {
        this.nota = nota;
    }

    public int getIdNota() {
        return idNota;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Rubrica getRubrica() {
        return rubrica;
    }

    public void setRubrica(Rubrica rubrica) {
        this.rubrica = rubrica;
    }
}
