package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.dto.ProfesorDto;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Servicio para autentificar a un profesor
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
@Service
@Transactional
public class ProfesorDetailService implements UserDetailsService {

    @Autowired
    ProfesorService profesorService;

    /**
     * Método que genera un objeto ProfesorDto mediante el nombre de usuario
     * @param correo El "nombre de usuario" del profesor a autentificar
     * @throws UsernameNotFoundException Excepcion para cuando no se encuentra el email ingresado
     * @return El número de ítems (números aleatorios) de que consta la serie
     */
    @Override
    public UserDetails loadUserByUsername(String correo) throws UsernameNotFoundException {
        Profesor profesor = profesorService.getByCorreo(correo).get();
        return ProfesorDto.build(profesor);
    }
}
