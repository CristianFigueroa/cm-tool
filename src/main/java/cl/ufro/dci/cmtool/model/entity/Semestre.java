package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Semestres")
public class Semestre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSemestre;

    @NotNull
    private int anioSemestre;

    @NotNull
    private int numeroSemestre;

    @NotNull
    private boolean activo;

    @JsonIgnore
    @OneToMany(mappedBy = "semestre", orphanRemoval = true)
    private Set<Modulo> modulos = new HashSet<>();

    public Semestre(){}

    public Semestre(@NotNull int anioSemestre, @NotNull int numeroSemestre) {
        this.anioSemestre = anioSemestre;
        this.numeroSemestre = numeroSemestre;
        this.activo = true;
    }

    public int getIdSemestre() {
        return idSemestre;
    }

    public int getNumeroSemestre() {
        return numeroSemestre;
    }

    public void setNumeroSemestre(int numeroSemestre) {
        this.numeroSemestre = numeroSemestre;
    }

    public int getAnioSemestre() {
        return anioSemestre;
    }

    public void setAnioSemestre(int anioSemestre) {
        this.anioSemestre = anioSemestre;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Set<Modulo> getModulos() {
        return modulos;
    }

    public void setModulos(Set<Modulo> modulos) {
        this.modulos = modulos;
    }
}
