package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Semestre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface SemestreRepository extends JpaRepository<Semestre, Integer> {

    Optional<Semestre> findByAnioSemestreAndNumeroSemestre(int anioSemestre, int numeroSemestre);

    Set<Semestre> findAllByOrderByAnioSemestreDescNumeroSemestreDesc();

    boolean existsByActivoTrue();

    Optional<Semestre> findByActivoTrue();

    boolean existsByAnioSemestreAndNumeroSemestre(int anioSemestre, int numeroSemestre);

}
