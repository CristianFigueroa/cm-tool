package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Evaluaciones")
public class Evaluacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idEvaluacion;

    @JsonFormat(pattern="dd-MM-yyyy")
    private Date fechaEvaluacion;

    @NotNull
    private int numeroEvaluacion;

    @NotNull
    private String descripcion;

    @NotNull
    private int numeroPlantilla;

    @ManyToOne
    @JoinColumn(name = "idProfesor_e", nullable = false, updatable = false)
    private Profesor profesor;

    @ManyToOne
    @JoinColumn(name = "idModulo_e", nullable = false, updatable = false)
    private Modulo modulo;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Alumnos_has_Evaluaciones",                //nombre tabla intermedia
            joinColumns = @JoinColumn(name = "idEvaluacion_ahe"),    // nombre columna relacionada con esta clase
            inverseJoinColumns = @JoinColumn(name = "idAlumno_ahe")  // nombre columna relacionada con la clase externa
    )
    private Set<Alumno> alumnos = new HashSet<>();

    @JsonIgnore
    @OneToMany (mappedBy = "evaluacion", orphanRemoval = true)
    private Set<Rubrica> rubricas = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "evaluacion", orphanRemoval = true)
    private Set<Nota> notas = new HashSet<>();

    public Evaluacion() {
    }

    public Evaluacion(Date fechaEvaluacion, @NotNull int numeroEvaluacion, @NotNull String descripcion,@NotNull int numeroPlantilla, @NotNull Modulo modulo) {
        this.fechaEvaluacion = fechaEvaluacion;
        this.numeroEvaluacion = numeroEvaluacion;
        this.numeroPlantilla = numeroPlantilla;
        this.descripcion = descripcion;
        this.modulo = modulo;
    }

    public int getIdEvaluacion() {
        return idEvaluacion;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public Set<Rubrica> getRubricas() {
        return rubricas;
    }

    public void setRubricas(Set<Rubrica> rubricas) {
        this.rubricas = rubricas;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public Set<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Set<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Set<Nota> getNotas() {
        return notas;
    }

    public void setNotas(Set<Nota> notas) {
        this.notas = notas;
    }

    public int getNumeroEvaluacion() {
        return numeroEvaluacion;
    }

    public void setNumeroEvaluacion(int numeroEvaluacion) {
        this.numeroEvaluacion = numeroEvaluacion;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public int getNumeroPlantilla() {
        return numeroPlantilla;
    }

    public void setNumeroPlantilla(int numeroPlantilla) {
        this.numeroPlantilla = numeroPlantilla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
