package cl.ufro.dci.cmtool.model.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.ufro.dci.cmtool.model.entity.TipoIndicador;

@Repository
public interface TipoIndicadorRepository extends JpaRepository<TipoIndicador, Integer> {

        boolean existsByIdTipoIndicador(int id);

        boolean existsByNombreIndicador(String nombre);

        Optional<TipoIndicador> findByNombreIndicador(String nombre);

        @Query(value = "" +
                        "SELECT * " +
                        "FROM tipos_indicadores as ti " +
                        "INNER JOIN tipos_indicadores_semanas as tis ON tis.tipos_indicadores_id_tipo_indicador = ti.id_tipo_indicador "
                        +
                        "WHERE tis.semanas = ?1 ", nativeQuery = true)
        Set<TipoIndicador> findByNivelIndicador(int nivel);

        @Query(value = "" +
                        "SELECT * " +
                        "FROM tipos_indicadores as ti " +
                        "INNER JOIN categorias as c ON c.id_categoria = ti.id_categoria_ti " +
                        "WHERE c.id_categoria = ?1", nativeQuery = true)
        Set<TipoIndicador> findByIdCategoria(int idCategoria);

        @Query(value = "SELECT  ti.nombre_indicador FROM tipos_indicadores ti", nativeQuery = true)
        Set<String> findDistinctIndicatorNames();

}
