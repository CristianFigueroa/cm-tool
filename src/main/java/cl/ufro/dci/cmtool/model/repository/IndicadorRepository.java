package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Indicador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IndicadorRepository extends JpaRepository<Indicador, Integer> {

    @Query(value = "" +
            "SELECT *" +
            "FROM indicadores " +
            "INNER JOIN rubricas as r ON r.id_rubrica = id_rubrica_i " +
            "WHERE r.id_rubrica = ?1",
            nativeQuery = true)
    Set<Indicador> findByRubrica(int id);

}
