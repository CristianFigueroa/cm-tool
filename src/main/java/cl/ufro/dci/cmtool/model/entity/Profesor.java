package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Profesores")
public class Profesor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProfesor;

    @NotNull
    private String nombreProfesor;

    @NotNull
    @Column(unique = true)
    private String correoProfesor;

    @JsonIgnore
    @NotNull
    private String contraseniaProfesor;

    @JsonIgnore
    private String contraseniaProfesorReal;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Profesores_has_Roles", // nombre tabla intermedia
            joinColumns = @JoinColumn(name = "idProfesor_phr"), // nombre columna relacionada con esta clase
            inverseJoinColumns = @JoinColumn(name = "idRol_phr") // nombre columna relacionada con la clase externa
    )
    private Set<Rol> rolProfesor = new HashSet<>();

    @OneToMany(mappedBy = "profesor", orphanRemoval = true)
    private Set<Modulo> modulos = new HashSet<>();

    public Profesor() {
    }

    public Profesor(@NotNull String nombreProfesor, @NotNull String correoProfesor,
            @NotNull String contraseniaProfesor, @NotNull String contraseniaProfesorReal) {
        this.nombreProfesor = nombreProfesor;
        this.correoProfesor = correoProfesor;
        this.contraseniaProfesor = contraseniaProfesor;
        this.contraseniaProfesorReal = contraseniaProfesorReal;
    }

    public Profesor(@NotNull String nombreProfesor, @NotNull String correoProfesor, @NotNull String contraseniaProfesor,
            Rol rol, @NotNull String contraseniaProfesorReal) {
        this.nombreProfesor = nombreProfesor;
        this.correoProfesor = correoProfesor;
        this.contraseniaProfesor = contraseniaProfesor;
        this.contraseniaProfesorReal = contraseniaProfesorReal;
        this.rolProfesor.add(rol);
    }

    public int getIdProfesor() {
        return idProfesor;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }

    public String getCorreoProfesor() {
        return correoProfesor;
    }

    public void setCorreoProfesor(String correoProfesor) {
        this.correoProfesor = correoProfesor;
    }

    public String getContraseniaProfesor() {
        return contraseniaProfesor;
    }

    public void setContraseniaProfesor(String contraseniaProfesor) {
        this.contraseniaProfesor = contraseniaProfesor;
    }

    public Set<Rol> getRolProfesor() {
        return rolProfesor;
    }

    public void setRolProfesor(Set<Rol> rolProfesor) {
        this.rolProfesor = rolProfesor;
    }

    public Set<Modulo> getModulos() {
        return modulos;
    }

    public void setModulos(Set<Modulo> modulos) {
        this.modulos = modulos;
    }
}
