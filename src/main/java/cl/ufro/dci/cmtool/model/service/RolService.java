package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Rol;
import cl.ufro.dci.cmtool.enums.NombreRol;
import cl.ufro.dci.cmtool.model.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RolService {

    @Autowired
    RolRepository rolRepository;

    public Optional<Rol> getByRolNombre(NombreRol nombreRol) {
        return rolRepository.findByNombreRol(nombreRol);
    }

    public void delete(int id) {
        rolRepository.deleteById(id);
    }

    public void save(Rol rol) {
        rolRepository.save(rol);
    }
}
