package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.configuration.Precarga;
import cl.ufro.dci.cmtool.model.entity.Rubrica;
import cl.ufro.dci.cmtool.model.repository.RubricaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class RubricaService {

    @Autowired
    RubricaRepository rubricaRepository;

    @Autowired
    Precarga precarga;

    public List<Rubrica> list() {
        return rubricaRepository.findAll();
    }

    public Optional<Rubrica> getByEvalAndAlumno(int evaluacion, String matriculaAlumno) {
        return rubricaRepository.findByEvalAndAlumno(evaluacion, matriculaAlumno);
    }

    public Set<Rubrica> listByAlumno(int matriculaAlumno) {
        return rubricaRepository.findByAlumno(matriculaAlumno);
    }

    public Optional<Rubrica> getById(int id) {
        return rubricaRepository.findById(id);
    }

    public void save(Rubrica rubrica) {
        rubricaRepository.save(rubrica);
    }

    public void saveAll(Iterable<Rubrica> rubricas) {
        rubricaRepository.saveAll(rubricas);
    }

    public void delete(int id) {
        rubricaRepository.deleteById(id);
    }

    public boolean existsById(int id) {
        return rubricaRepository.existsById(id);
    }

    // servicio que retorna lista de strings de RA
    public List<String> obtenerRA() {
        return precarga.obtenerRA();
    }

    // servicio que retorna lista de string de Indicadores
    public List<String> obtenerIndicador() {
        return precarga.obtenerIndicador();
    }

    // servicio que retorna map de ra y indicadores
    public List<Map<String, Object>> obtenerRAConIndicadores() {
        return precarga.obtenerRAConIndicadores();
    }
}
