package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Indicador;
import cl.ufro.dci.cmtool.model.repository.IndicadorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class IndicadorService {

    @Autowired
    IndicadorRepository indicadorRepository;

    public Optional<Indicador> getById(int id) {
        return indicadorRepository.findById(id);
    }

    public void save (Indicador indicador){
        indicadorRepository.save(indicador);
    }

    public void saveAll (Iterable<Indicador> indicadores){
        indicadorRepository.saveAll(indicadores);
    }

    public void delete (int id){
        indicadorRepository.deleteById(id);
    }

    public Set<Indicador> getByRubrica (int idRubrica){
        return indicadorRepository.findByRubrica(idRubrica);
    }

}
