package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Rubricas")
public class Rubrica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idRubrica;

    @OneToMany(mappedBy = "rubrica" , orphanRemoval = true)
    private Set<Indicador> indicadores = new HashSet<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idAlumno_r", nullable = false, updatable = false)
    private Alumno alumno;

    @ManyToOne
    @JoinColumn(name = "idEvaluacion_r", nullable = false, updatable = false)
    private Evaluacion evaluacion;

    @ManyToOne
    @JoinColumn(name = "idModulo_r", nullable = false, updatable = false)
    private Modulo modulo;

    public Rubrica() {
    }

    public int getIdRubrica() {
        return idRubrica;
    }

    public Set<Indicador> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(Set<Indicador> indicadores) {
        this.indicadores = indicadores;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }


}
