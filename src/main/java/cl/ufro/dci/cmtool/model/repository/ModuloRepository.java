package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Modulo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ModuloRepository extends JpaRepository<Modulo, Integer> {

    Optional<Modulo> findByCodigoModuloAndSemestre_ActivoTrue(String codigoModulo);

    Set<Modulo> findAllBySemestre_ActivoTrue();

    boolean existsByCodigoModuloAndSemestre_ActivoTrue(String codigoModulo);

    Optional<Modulo> findByProfesor_CorreoProfesorAndSemestreActivoTrue(String correoProfesor);

    boolean existsByProfesor_CorreoProfesorAndSemestreActivoTrue(String correoProfesor);

    Optional<Modulo> findByProfesor_IdProfesorAndSemestre_ActivoTrue(int idProfesor);

    boolean existsByProfesor_IdProfesorAndSemestre_ActivoTrue(int idProfesor);

}
