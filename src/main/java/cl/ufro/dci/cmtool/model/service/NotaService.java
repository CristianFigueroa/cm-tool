package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Nota;
import cl.ufro.dci.cmtool.model.repository.NotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class NotaService {

    @Autowired
    NotaRepository notaRepository;

    public Optional<Nota> getById(int id) {
        return notaRepository.findById(id);
    }

    public Set<Nota> getByAlumno(String matricula){
        return notaRepository.findByAlumno(matricula);
    }

    public void save (Nota nota){
        notaRepository.save(nota);
    }

    public void delete (int id){
        notaRepository.deleteById(id);
    }
}
