package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Rol;
import cl.ufro.dci.cmtool.enums.NombreRol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
    Optional<Rol> findByNombreRol(NombreRol nombreRol);
}