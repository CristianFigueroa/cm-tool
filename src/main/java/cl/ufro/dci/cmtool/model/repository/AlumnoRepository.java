package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Profesor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, String> {

        Optional<Alumno> findByMatriculaAlumno(String matriculaAlumno);

        boolean existsByMatriculaAlumno(String matriculaAlumno);

        @Query(value = "" +
                        "SELECT matricula_alumno, codigo_carrera, nombre_alumno " +
                        "FROM alumnos " +
                        "INNER JOIN alumnos_has_modulos ahm on alumnos.matricula_alumno = ahm.id_alumno_ahm " +
                        "INNER JOIN modulos m on ahm.id_modulo_ahm = m.id_modulo " +
                        "INNER JOIN semestres s on id_semestre_m = s.id_semestre " +
                        "INNER JOIN profesores p on m.id_profesor_m = p.id_profesor " +
                        "WHERE s.activo = true and p.correo_profesor = ?1 " +
                        "ORDER BY nombre_alumno ASC ", nativeQuery = true)
        Set<Alumno> findAllByCorreoProfesor(String correoProfesor);

        @Query(value = "" +
                        "SELECT matricula_alumno, codigo_carrera, nombre_alumno " +
                        "FROM alumnos " +
                        "INNER JOIN alumnos_has_modulos ahm on alumnos.matricula_alumno = ahm.id_alumno_ahm " +
                        "INNER JOIN modulos m on ahm.id_modulo_ahm = m.id_modulo " +
                        "INNER JOIN semestres s on id_semestre_m = s.id_semestre " +
                        "INNER JOIN profesores p on m.id_profesor_m = p.id_profesor " +
                        "WHERE s.activo = true and p.id_profesor = ?1 " +
                        "ORDER BY nombre_alumno ASC ", nativeQuery = true)
        Set<Alumno> findAllByIdProfesor(int idProfesor);

        @Query(value = "SELECT * FROM profesores WHERE id_profesor = ?1", nativeQuery = true)
        List<Object[]> findAllByIDProfesor(int idProfesor);
}
