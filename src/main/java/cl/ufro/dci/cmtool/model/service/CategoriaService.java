package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Categoria;
import cl.ufro.dci.cmtool.model.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoriaService {

    @Autowired
    CategoriaRepository categoriaRepository;

    public void save (Categoria categoria){
        categoriaRepository.save(categoria);
    }

    public List<Categoria> listAll() {
        return categoriaRepository.findAll();
    }

    public void delete (int id){
        categoriaRepository.deleteById(id);
    }

    public boolean existByNombre(String nombre){
        return categoriaRepository.existsByNombreCategoria(nombre);
    }

    public boolean existById(int id){
        return categoriaRepository.existsById(id);
    }

    public Optional<Categoria> getById(int id) {
        return categoriaRepository.findById(id);
    }

    public Optional<Categoria> getByNombreCategoria(String nombre) {
        return categoriaRepository.findByNombreCategoria(nombre);
    }

}
