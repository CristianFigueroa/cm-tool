package cl.ufro.dci.cmtool.model.entity;

import cl.ufro.dci.cmtool.enums.NombreRol;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity(name = "Roles")
public class Rol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idRol;

    @NotNull
    @Enumerated(EnumType.STRING) //por defecto la base de datos crea enumeraciones de numero, esta es de cadenas
    private NombreRol nombreRol;

    public Rol() {
    }

    public Rol(NombreRol nombreRol) {
        this.nombreRol = nombreRol;
    }

    public int getId() {
        return idRol;
    }

    public void setId(int idRol) {
        this.idRol = idRol;
    }

    public NombreRol getRolNombre() {
        return nombreRol;
    }

    public void setRolNombre(NombreRol nombreRol) {
        this.nombreRol = nombreRol;
    }
}
