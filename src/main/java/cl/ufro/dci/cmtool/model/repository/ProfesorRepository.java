package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Profesor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, Integer> {
    Optional<Profesor> findByNombreProfesor(String nombreProfesor);

    Optional<Profesor> findByCorreoProfesor(String correo);

    boolean existsByCorreoProfesor(String correo);


}
