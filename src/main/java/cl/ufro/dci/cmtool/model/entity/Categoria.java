package cl.ufro.dci.cmtool.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Categorias")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCategoria;

    @NotNull
    private String nombreCategoria;

    public Categoria() {
    }

    public Categoria(@NotNull String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

}
