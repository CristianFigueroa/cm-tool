package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Semestre;
import cl.ufro.dci.cmtool.model.repository.SemestreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class SemestreService {

    @Autowired
    SemestreRepository semestreRepository;

    public void save (Semestre semestre){
        semestreRepository.save(semestre);
    }

    public Set<Semestre> listAll() {
        return semestreRepository.findAllByOrderByAnioSemestreDescNumeroSemestreDesc();
    }

    public void delete (int idSemestre){
        semestreRepository.deleteById(idSemestre);
    }

    public boolean existByAnioSemestreAndNumeroSemestre(int anioSemestre, int numeroSemestre){
        return semestreRepository.existsByAnioSemestreAndNumeroSemestre(anioSemestre, numeroSemestre);
    }

    public Optional<Semestre> getByAnioSemestreAndNumeroSemestre(int anioSemestre, int numeroSemestre) {
        return semestreRepository.findByAnioSemestreAndNumeroSemestre(anioSemestre, numeroSemestre);
    }

    public boolean existActivo(){
        return semestreRepository.existsByActivoTrue();
    }

    public Optional<Semestre> getActivo() {
        return semestreRepository.findByActivoTrue();
    }

    public Optional<Semestre> getByIdSemestre(int idSemestre) {
        return semestreRepository.findById(idSemestre);
    }

    public boolean existByIdSemestre(int idSemestre){
        return semestreRepository.existsById(idSemestre);
    }

}
