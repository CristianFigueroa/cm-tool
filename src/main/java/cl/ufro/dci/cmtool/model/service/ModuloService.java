package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Modulo;
import cl.ufro.dci.cmtool.model.repository.ModuloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class ModuloService {

    @Autowired
    ModuloRepository moduloRepository;

    public void save (Modulo modulo){
        moduloRepository.save(modulo);
    }

    public Set<Modulo> listAllActivo() {
        return moduloRepository.findAllBySemestre_ActivoTrue();
    }

    public void delete (int idModulo){
        moduloRepository.deleteById(idModulo);
    }

    public boolean existByCodigoModulo(String codigoModulo){
        return moduloRepository.existsByCodigoModuloAndSemestre_ActivoTrue(codigoModulo);
    }

    public Optional<Modulo> getByCodigoModulo(String codigoModulo) {
        return moduloRepository.findByCodigoModuloAndSemestre_ActivoTrue(codigoModulo);
    }

    public boolean existActiveByEmailProfesor(String correoProfesor){
        return moduloRepository.existsByProfesor_CorreoProfesorAndSemestreActivoTrue(correoProfesor);
    }

    public Optional<Modulo> getActiveByCorreoProfesor(String correoProfesor) {
        return moduloRepository.findByProfesor_CorreoProfesorAndSemestreActivoTrue(correoProfesor);
    }

    public boolean existActiveByIdProfesor(int idProfesor){
        return moduloRepository.existsByProfesor_IdProfesorAndSemestre_ActivoTrue(idProfesor);
    }

    public Optional<Modulo> getActiveByIdProfesor(int idProfesor) {
        return moduloRepository.findByProfesor_IdProfesorAndSemestre_ActivoTrue(idProfesor);
    }
}
