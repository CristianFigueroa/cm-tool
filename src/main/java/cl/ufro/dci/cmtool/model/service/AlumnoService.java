package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.repository.AlumnoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class AlumnoService {

    @Autowired
    AlumnoRepository alumnoRepository;

    public Set<Alumno> listAll() {
        return (Set<Alumno>) alumnoRepository.findAll();
    }

    public Set<Alumno> listAllByCorreoProfesor(String correoProfesor) {
        return alumnoRepository.findAllByCorreoProfesor(correoProfesor);
    }

    public Set<Alumno> listAllByIdProfesor(int idProfesor) {
        return alumnoRepository.findAllByIdProfesor(idProfesor);
    }

    public Optional<Alumno> getByMatricula(String matricula) {
        return alumnoRepository.findByMatriculaAlumno(matricula);
    }

    public void save(Alumno alumno) {
        alumnoRepository.save(alumno);
    }

    public void saveAll(Iterable<Alumno> alumnos) {
        alumnoRepository.saveAll(alumnos);
    }

    public void delete(String matricula) {
        alumnoRepository.deleteById(matricula);
    }

    public boolean existByMatricula(String matricula) {
        return alumnoRepository.existsByMatriculaAlumno(matricula);
    }

    public List<Object[]> listAllByIDProfesor(int idProfesor) {
        return alumnoRepository.findAllByIDProfesor(idProfesor);
    }

}
