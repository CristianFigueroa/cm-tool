package cl.ufro.dci.cmtool.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity(name = "TiposIndicadores")
public class TipoIndicador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTipoIndicador;

    @NotNull
    @Column(unique = true)
    private String nombreIndicador;

    private int nivelIndicador;

    @ElementCollection
    private List<Integer> semanas;

    @ManyToOne
    @JoinColumn(name = "idCategoria_ti")
    private Categoria categoria;

    private String criterio0;
    private String criterio1;
    private String criterio2;
    private String criterio3;
    private String criterio4;

    public TipoIndicador() {
    }

    public TipoIndicador(@NotNull String nombreIndicador, Categoria categoria) {
        this.nombreIndicador = nombreIndicador;
        this.categoria = categoria;
    }

    public List<Integer> getSemanas() {
        return semanas;
    }

    public void setSemanas(List<Integer> semanas) {
        this.semanas = semanas;
    }

    public void setIdTipoIndicador(int idTipoIndicador) {
        this.idTipoIndicador = idTipoIndicador;
    }

    public int getIdTipoIndicador() {
        return idTipoIndicador;
    }

    public String getNombreIndicador() {
        return nombreIndicador;
    }

    public void setNombreIndicador(String nombreIndicador) {
        this.nombreIndicador = nombreIndicador;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public int getNivelIndicador() {
        return nivelIndicador;
    }

    public void setNivelIndicador(int nivelIndicador) {
        this.nivelIndicador = nivelIndicador;
    }

    public String getCriterio0() {
        return criterio0;
    }

    public void setCriterio0(String criterio0) {
        this.criterio0 = criterio0;
    }

    public String getCriterio1() {
        return criterio1;
    }

    public void setCriterio1(String criterio1) {
        this.criterio1 = criterio1;
    }

    public String getCriterio2() {
        return criterio2;
    }

    public void setCriterio2(String criterio2) {
        this.criterio2 = criterio2;
    }

    public String getCriterio3() {
        return criterio3;
    }

    public void setCriterio3(String criterio3) {
        this.criterio3 = criterio3;
    }

    public String getCriterio4() {
        return criterio4;
    }

    public void setCriterio4(String criterio4) {
        this.criterio4 = criterio4;
    }
}
