package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Evaluacion;
import cl.ufro.dci.cmtool.model.repository.EvaluacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class EvaluacionService {

    @Autowired
    EvaluacionRepository evaluacionRepository;

    public List<Evaluacion> listAll() {
        return evaluacionRepository.findAll();
    }

    public Optional<Evaluacion> getById(int idEval) {
        return evaluacionRepository.findById(idEval);
    }

    public Set<Evaluacion> listAllByProfesor(String correoProfesor) {
        return evaluacionRepository.findByModulo_Profesor_CorreoProfesorAndModulo_Semestre_ActivoTrue(correoProfesor);
    }

    public boolean existById(int id) {
        return evaluacionRepository.existsById(id);
    }

    public void save(Evaluacion evaluacion) {
        evaluacionRepository.save(evaluacion);
    }

    public void delete(int id) {
        evaluacionRepository.deleteById(id);
    }

    public Set<Object[]> findEvaluacionPuntajePorIndicadorMatricula(String nombreIndicador, String matriculaAlumno) {
        return evaluacionRepository.findEvaluacionPuntajePorIndicadorMatricula(nombreIndicador, matriculaAlumno);
    }

    public Set<Evaluacion> findByDescripcion(String descripcion) {
        return evaluacionRepository.findByDescripcion(descripcion);
    }

}
