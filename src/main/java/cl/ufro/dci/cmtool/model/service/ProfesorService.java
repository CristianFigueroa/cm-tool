package cl.ufro.dci.cmtool.model.service;

import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.repository.ProfesorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProfesorService {

    @Autowired
    ProfesorRepository profesorRepository;

    public List<Profesor> list() {
        return profesorRepository.findAll();
    }

    public Optional<Profesor> getById(int id) {
        return profesorRepository.findById(id);
    }

    public Optional<Profesor> getByCorreo(String correo) {
        return profesorRepository.findByCorreoProfesor(correo);
    }

    public void save(Profesor profesor) {
        profesorRepository.save(profesor);
    }

    public void delete(int id) {
        profesorRepository.deleteById(id);
    }

    public boolean existsById(int id) {
        return profesorRepository.existsById(id);
    }

    public boolean existsByEmail(String email) {
        return profesorRepository.existsByCorreoProfesor(email);
    }
}
