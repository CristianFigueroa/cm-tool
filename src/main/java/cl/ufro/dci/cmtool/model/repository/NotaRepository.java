package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Nota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface NotaRepository extends JpaRepository<Nota,Integer> {

    @Query(value = "" +
            "SELECT *" +
            "FROM notas " +
            "INNER JOIN alumnos as a ON a.matricula_alumno = id_alumno_n " +
            "WHERE a.matricula_alumno = ?1",
            nativeQuery = true)
    Set<Nota> findByAlumno(String matricula);

}