package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Modulos")
public class Modulo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idModulo;

    @NotNull
    private String codigoModulo;

    @ManyToOne
    @JoinColumn(name = "idSemestre_m", nullable = false, updatable = false)
    private Semestre semestre;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idProfesor_m", nullable = false, updatable = false)
    private Profesor profesor;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Alumnos_has_Modulos",                //nombre tabla intermedia
            joinColumns = @JoinColumn(name = "idModulo_ahm"),    // nombre columna relacionada con esta clase
            inverseJoinColumns = @JoinColumn(name = "idAlumno_ahm")  // nombre columna relacionada con la clase externa
    )
    private Set<Alumno> alumnos = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "modulo", orphanRemoval = true)
    private Set<Evaluacion> evaluaciones = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "modulo", orphanRemoval = true)
    private Set<Rubrica> rubricas = new HashSet<>();

    public Modulo(){
    }

    public Modulo(@NotNull String codigoModulo, Semestre semestre, Profesor profesor) {
        this.codigoModulo = codigoModulo;
        this.semestre = semestre;
        this.profesor = profesor;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public String getCodigoModulo() {
        return codigoModulo;
    }

    public void setCodigoModulo(String codigoModulo) {
        this.codigoModulo = codigoModulo;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public Set<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Set<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Set<Evaluacion> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(Set<Evaluacion> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public Set<Rubrica> getRubricas() {
        return rubricas;
    }

    public void setRubricas(Set<Rubrica> rubricas) {
        this.rubricas = rubricas;
    }
}
