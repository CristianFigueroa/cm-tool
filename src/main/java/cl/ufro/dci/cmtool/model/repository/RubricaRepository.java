package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Rubrica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface RubricaRepository extends JpaRepository <Rubrica, Integer> {


    @Query(value = "" +
            "SELECT *" +
            "FROM rubricas " +
            "INNER JOIN alumnos as a ON a.matricula_alumno = id_alumno_r " +
            "WHERE a.matricula_alumno = ?1",
            nativeQuery = true)
    Set<Rubrica> findByAlumno(int matriculaAlumno);

    @Query(value = "" +
            "SELECT *" +
            "FROM rubricas " +
            "INNER JOIN alumnos as a ON a.matricula_alumno = id_alumno_r " +
            "INNER JOIN evaluaciones as e ON e.id_evaluacion = id_evaluacion_r " +
            "WHERE e.id_evaluacion = ?1 and a.matricula_alumno = ?2",
            nativeQuery = true)
    Optional<Rubrica> findByEvalAndAlumno(int idEval, String matriculaAlumno);
}
