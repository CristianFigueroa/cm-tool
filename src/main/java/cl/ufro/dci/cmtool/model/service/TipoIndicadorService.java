package cl.ufro.dci.cmtool.model.service;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ufro.dci.cmtool.model.entity.TipoIndicador;
import cl.ufro.dci.cmtool.model.repository.TipoIndicadorRepository;

@Service
@Transactional
public class TipoIndicadorService {

    @Autowired
    TipoIndicadorRepository tipoIndicadorRepository;

    public void save(TipoIndicador tipoIndicador) {
        tipoIndicadorRepository.save(tipoIndicador);
    }

    public boolean existById(int id) {
        return tipoIndicadorRepository.existsByIdTipoIndicador(id);
    }

    public boolean existByNombre(String nombre) {
        return tipoIndicadorRepository.existsByNombreIndicador(nombre);
    }

    public Optional<TipoIndicador> getById(int id) {
        return tipoIndicadorRepository.findById(id);
    }

    public Optional<TipoIndicador> getByNombre(String nombre) {
        return tipoIndicadorRepository.findByNombreIndicador(nombre);
    }

    public Set<TipoIndicador> getByNivel(int nivel) {
        return tipoIndicadorRepository.findByNivelIndicador(nivel);
    }

    public Set<TipoIndicador> getByIdCategoria(int idCategoria) {
        return tipoIndicadorRepository.findByIdCategoria(idCategoria);
    }

    public Set<String> getDistinctIndicatorNames() {
        return tipoIndicadorRepository.findDistinctIndicatorNames();
    }

}
