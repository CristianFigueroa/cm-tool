package cl.ufro.dci.cmtool.model.repository;

import cl.ufro.dci.cmtool.model.entity.Evaluacion;
import cl.ufro.dci.cmtool.model.entity.Indicador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface EvaluacionRepository extends JpaRepository<Evaluacion, Integer> {

    Set<Evaluacion> findByModulo_Profesor_CorreoProfesorAndModulo_Semestre_ActivoTrue(String correoProfesor);

    @Query(value = "SELECT ind.puntaje_indicador, e.descripcion " +
            "FROM tipos_indicadores ti " +
            "INNER JOIN indicadores ind ON ti.id_tipo_indicador = ind.id_tipo_indicador_i " +
            "INNER JOIN rubricas r ON r.id_rubrica = ind.id_rubrica_i " +
            "INNER JOIN alumnos a ON a.matricula_alumno = r.id_alumno_r " +
            "INNER JOIN evaluaciones e ON e.id_evaluacion = r.id_evaluacion_r " +
            "WHERE ti.nombre_indicador = ?1 " +
            "AND a.matricula_alumno = ?2 " +
            "ORDER BY e.fecha_evaluacion", nativeQuery = true)
    Set<Object[]> findEvaluacionPuntajePorIndicadorMatricula(String nombreIndicador, String matriculaAlumno);

    @Query(value = "" +
            "SELECT * " +
            "FROM evaluaciones e " +
            "WHERE e.descripcion = ?1", nativeQuery = true)
    Set<Evaluacion> findByDescripcion(String descripcion);

}
