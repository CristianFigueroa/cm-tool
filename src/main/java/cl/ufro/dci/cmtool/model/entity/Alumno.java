package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = "Alumnos")
public class Alumno {

    @Id
    @Column(unique = true)
    private String matriculaAlumno;

    @NotNull
    private String nombreAlumno;

    @NotNull
    private int codigoCarrera;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Alumnos_has_Evaluaciones",                //nombre tabla intermedia
            joinColumns = @JoinColumn(name = "idAlumno_ahe"),    // nombre columna relacionada con esta clase
            inverseJoinColumns = @JoinColumn(name = "idEvaluacion_ahe")  // nombre columna relacionada con la clase externa
    )
    private Set<Evaluacion> evaluaciones = new HashSet<>();

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Alumnos_has_Modulos",                //nombre tabla intermedia
            joinColumns = @JoinColumn(name = "idAlumno_ahm"),    // nombre columna relacionada con esta clase
            inverseJoinColumns = @JoinColumn(name = "idModulo_ahm")  // nombre columna relacionada con la clase externa
    )
    private Set<Modulo> modulos = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "alumno")
    private Set<Nota> notas = new HashSet<>();

    @OneToMany(mappedBy = "alumno", orphanRemoval = true)
    private Set<Rubrica> rubricas = new HashSet<>();

    public Alumno() {
    }

    public Alumno(@NotNull String matriculaAlumno, @NotNull String nombreAlumno, @NotNull int codigoCarrera, @NotNull Modulo modulo) {
        this.matriculaAlumno = matriculaAlumno;
        this.nombreAlumno = nombreAlumno;
        this.codigoCarrera = codigoCarrera;
        getModulos().add(modulo);
    }

    public String getMatriculaAlumno() {
        return matriculaAlumno;
    }

    public void setMatriculaAlumno(String matriculaAlumno) {
        this.matriculaAlumno = matriculaAlumno;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public Set<Evaluacion> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(Set<Evaluacion> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public Set<Nota> getNotas() {
        return notas;
    }

    public void setNotas(Set<Nota> notas) {
        this.notas = notas;
    }

    public Set<Rubrica> getRubricas() {
        return rubricas;
    }

    public Set<Rubrica> filterRubricasActuales() {
        return rubricas.stream().filter(x -> x.getModulo().getSemestre().isActivo()).collect(Collectors.toSet());
    }

    public void setRubricas(Set<Rubrica> rubricas) {
        this.rubricas = rubricas;
    }

    public int getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(int codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }

    public Set<Modulo> getModulos() {
        return modulos;
    }

    public void setModulos(Set<Modulo> modulos) {
        this.modulos = modulos;
    }

    public void addModulo(Modulo modulo) {
        this.modulos.add(modulo);
    }


}
