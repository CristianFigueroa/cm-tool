package cl.ufro.dci.cmtool.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Indicadores")
public class Indicador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIndicador;

    @NotNull
    private int puntajeIndicador;

    @ManyToOne
    @JoinColumn(name = "idTipoIndicador_i", nullable = false, updatable = false)
    private TipoIndicador tipoIndicador;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idRubrica_i", nullable = false, updatable = false)
    private Rubrica rubrica;

    public Indicador() {
    }

    public Indicador(
            @NotNull TipoIndicador tipoIndicador,
            @NotNull int puntajeIndicador) {
        this.puntajeIndicador = puntajeIndicador;
        this.tipoIndicador = tipoIndicador;
    }

    public int getIdIndicador() {
        return idIndicador;
    }

    public TipoIndicador getTipoIndicador() {
        return tipoIndicador;
    }

    public void setTipoIndicador(TipoIndicador tipoIndicador) {
        this.tipoIndicador = tipoIndicador;
    }

    public int getPuntajeIndicador() {
        return puntajeIndicador;
    }

    public void setPuntajeIndicador(int puntajeIndicador) {
        this.puntajeIndicador = puntajeIndicador;
    }

    public Rubrica getRubrica() {
        return rubrica;
    }

    public void setRubrica(Rubrica rubrica) {
        this.rubrica = rubrica;
    }

}
