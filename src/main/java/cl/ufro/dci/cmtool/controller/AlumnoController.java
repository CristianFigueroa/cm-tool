package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.configuration.JwtProvider;
import cl.ufro.dci.cmtool.dto.AlumnoDto;
import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.helper.GestorExcelAlumnos;
import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Modulo;
import cl.ufro.dci.cmtool.model.entity.Nota;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping("/alumno")
public class AlumnoController extends GenericoController {

    @Autowired
    AlumnoService alumnoService;

    @Autowired
    ModuloService moduloService;

    @Autowired
    RubricaService rubricaService;

    @Autowired
    NotaService notaService;

    @Autowired
    EvaluacionService evaluacionService;

    @Autowired
    ProfesorService profesorService;

    @Autowired
    JwtProvider jwtProvider;

    @GetMapping("/listaAdmin/{idProfesor}")
    public ResponseEntity<Set<Alumno>> listAdmin(@PathVariable("idProfesor") int idProfesor) {
        Set<Alumno> list = alumnoService.listAllByIdProfesor(idProfesor);
        return new ResponseEntity<Set<Alumno>>(list, HttpStatus.OK);
    }

    @GetMapping("/listaTodos")
    public ResponseEntity<Set<Alumno>> listAll() {
        Set<Alumno> list = alumnoService.listAll();
        return new ResponseEntity<Set<Alumno>>(list, HttpStatus.OK);
    };

    @GetMapping("/lista")
    public ResponseEntity<Set<Alumno>> list(@RequestHeader(name = "Authorization") String token) {
        String emailProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        if (!moduloService.existActiveByEmailProfesor(emailProfesor)) {
            return new ResponseEntity(new Mensaje("El docente no esta vinculado a un módulo"), HttpStatus.BAD_REQUEST);
        }
        Set<Alumno> list = alumnoService.listAllByCorreoProfesor(emailProfesor);
        return new ResponseEntity<Set<Alumno>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{matricula}")
    public ResponseEntity<Alumno> getByMatricula(@PathVariable("matricula") String matricula) {
        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        Alumno alumno = alumnoService.getByMatricula(matricula).get();
        alumno.setRubricas(alumno.filterRubricasActuales());
        return new ResponseEntity<Alumno>(alumno, HttpStatus.OK);
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @PutMapping("/actualizar/{matricula}/nuevoNombre/{nuevoNombre}")
    public ResponseEntity<?> update(@PathVariable("matricula") String matricula,
            @PathVariable("nuevoNombre") String nuevoNombre) {
        Alumno alumno = alumnoService.getByMatricula(matricula).get();
        alumno.setNombreAlumno(nuevoNombre);
        alumnoService.save(alumno);
        return new ResponseEntity<>(new Mensaje("Estudiante editado correctamente"), HttpStatus.OK);
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @PostMapping("/nuevo")
    public ResponseEntity<?> create(@RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody AlumnoDto alumnoDto,
            BindingResult result) {
        String correoProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        if (!moduloService.existActiveByEmailProfesor(correoProfesor))
            return new ResponseEntity<>(new Mensaje("El docente no esta vinculado a un módulo"),
                    HttpStatus.BAD_REQUEST);
        if (alumnoService.existByMatricula(alumnoDto.getMatricula())) {
            Alumno alumno = alumnoService.getByMatricula(alumnoDto.getMatricula()).get();
            if (alumno.getModulos().stream().anyMatch(modulo -> modulo.getSemestre().isActivo() == true)) {
                if (alumno.getModulos().stream()
                        .anyMatch(modulo -> (modulo.getProfesor().getCorreoProfesor() == correoProfesor
                                && modulo.getSemestre().isActivo() == true))) {
                    return new ResponseEntity<>(new Mensaje("El estudiante ya existe en este módulo"),
                            HttpStatus.BAD_REQUEST);
                } else {
                    Modulo modulo = alumno.getModulos().stream().filter(x -> x.getSemestre().isActivo() == true)
                            .findAny().get();
                    return new ResponseEntity<>(
                            new Mensaje("El estudiante está vinculado al módulo: " + modulo.getCodigoModulo()
                                    + " del docente: " + modulo.getProfesor().getNombreProfesor()),
                            HttpStatus.BAD_REQUEST);
                }
            } else {
                Set<Modulo> moduloSet = alumno.getModulos();
                moduloSet.add(moduloService.getActiveByCorreoProfesor(correoProfesor).get());
                alumnoService.save(alumno);
                return new ResponseEntity<>(new Mensaje("Estudiante creado correctamente"), HttpStatus.CREATED);
            }
        }
        Alumno alumno = new Alumno(alumnoDto.getMatricula(), alumnoDto.getNombre(), alumnoDto.getCodigoCarrera(),
                moduloService.getActiveByCorreoProfesor(correoProfesor).get());
        alumnoService.save(alumno);
        return new ResponseEntity<>(new Mensaje("Estudiante creado correctamente"), HttpStatus.CREATED);
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @PostMapping("/nuevoAdmin/{idProfesor}")
    public ResponseEntity<?> createAdmin(@PathVariable("idProfesor") int idProfesor,
            @Valid @RequestBody AlumnoDto alumnoDto,
            BindingResult result) {
        if (!profesorService.existsById(idProfesor))
            return new ResponseEntity<>(new Mensaje("El docente no existe"), HttpStatus.BAD_REQUEST);
        if (!moduloService.existActiveByIdProfesor(idProfesor))
            return new ResponseEntity<>(new Mensaje("El docente no esta vinculado a un módulo"),
                    HttpStatus.BAD_REQUEST);
        if (alumnoService.existByMatricula(alumnoDto.getMatricula())) {
            Alumno alumno = alumnoService.getByMatricula(alumnoDto.getMatricula()).get();
            if (alumno.getModulos().stream().anyMatch(modulo -> modulo.getSemestre().isActivo() == true)) {
                if (alumno.getModulos().stream().anyMatch(modulo -> (modulo.getProfesor().getIdProfesor() == idProfesor
                        && modulo.getSemestre().isActivo() == true))) {
                    return new ResponseEntity<>(new Mensaje("El estudiante ya existe en este módulo"),
                            HttpStatus.BAD_REQUEST);
                } else {
                    Modulo modulo = alumno.getModulos().stream().filter(x -> x.getSemestre().isActivo() == true)
                            .findAny().get();
                    return new ResponseEntity<>(
                            new Mensaje("El estudiante está vinculado al módulo: " + modulo.getCodigoModulo()
                                    + " del docente: " + modulo.getProfesor().getNombreProfesor()),
                            HttpStatus.BAD_REQUEST);
                }
            } else {
                Set<Modulo> moduloSet = alumno.getModulos();
                moduloSet.add(moduloService.getActiveByIdProfesor(idProfesor).get());
                alumnoService.save(alumno);
                return new ResponseEntity<>(new Mensaje("Estudiante creado correctamente"), HttpStatus.CREATED);
            }
        }
        Alumno alumno = new Alumno(alumnoDto.getMatricula(), alumnoDto.getNombre(), alumnoDto.getCodigoCarrera(),
                moduloService.getActiveByIdProfesor(idProfesor).get());
        alumnoService.save(alumno);
        return new ResponseEntity<>(new Mensaje("Estudiante creado correctamente"), HttpStatus.CREATED);
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @PostMapping("/nuevos/{idProfesor}")
    public @ResponseBody ResponseEntity<?> createByExel(@PathVariable("idProfesor") int idProfesor,
            @RequestParam("file") MultipartFile file) throws Exception {
        if (!profesorService.existsById(idProfesor))
            return new ResponseEntity<>(new Mensaje("No se encontró al docente"), HttpStatus.BAD_REQUEST);
        if (!moduloService.existActiveByIdProfesor(idProfesor))
            return new ResponseEntity<>(new Mensaje("El docente no esta vinculado a un módulo"),
                    HttpStatus.BAD_REQUEST);
        Modulo modulo = moduloService.getActiveByIdProfesor(idProfesor).get();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Path path = Files.createTempFile("", file.getOriginalFilename());
                Files.write(path, bytes);
                List<Alumno> alumnos = GestorExcelAlumnos.read(path.toString());
                for (Alumno alumno : alumnos) {
                    if (!alumnoService.existByMatricula(alumno.getMatriculaAlumno())) {
                        alumno.addModulo(modulo);
                        alumnoService.save(alumno);
                    } else {
                        // se cambia de modulo
                    }
                }
                Files.delete(path);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(new Mensaje("Error en la carga del archivo"), HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(new Mensaje("Ok"), HttpStatus.OK);
        }
        return new ResponseEntity<>(new Mensaje("No hay un archivo seleccionado"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/agregar/{idProfesor}/{nombreAlumno}/{rutAlumno}")
    public ResponseEntity<?> addNewStudent(@PathVariable("idProfesor") int idProfesor,
            @PathVariable("nombreAlumno") String nombreAlumno,
            @PathVariable("rutAlumno") String rutAlumno) {
        // Buscar al profesor por su ID
        if (!profesorService.existsById(idProfesor)) {
            return new ResponseEntity<>(new Mensaje("No se encontró al docente"), HttpStatus.BAD_REQUEST);
        }

        // Crear un nuevo alumno
        Alumno alumno = new Alumno();
        alumno.setNombreAlumno(nombreAlumno);
        alumno.setMatriculaAlumno(rutAlumno);
        alumno.setCodigoCarrera(1234); // Código de carrera fijo
        Modulo modulo = moduloService.getActiveByIdProfesor(idProfesor).get();

        // Guardar el alumno y el profesor en la base de datos
        if (!alumnoService.existByMatricula(alumno.getMatriculaAlumno())) {
            alumno.addModulo(modulo);
            alumnoService.save(alumno);
            return new ResponseEntity<>(new Mensaje("Estudiante agregado correctamente al profesor"), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(new Mensaje("El estudiante ya existe"), HttpStatus.BAD_REQUEST);

        }
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @DeleteMapping("/eliminar/{matricula}")
    public ResponseEntity<?> deleteById(@PathVariable("matricula") String matricula) {
        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity<>(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        alumnoService.delete(matricula);
        return new ResponseEntity<>(new Mensaje("Estudiante eliminado correctamente"), HttpStatus.OK);
    }

    // @PreAuthorize("hasRole('PROFESOR')")
    @GetMapping("/notas/{matricula}")
    public ResponseEntity<Set<Nota>> listarNota(@PathVariable("matricula") String matricula) {
        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        Set<Nota> list = notaService.getByAlumno(matricula);
        return new ResponseEntity<Set<Nota>>(list, HttpStatus.OK);
    }

    @GetMapping("/modulo/{matricula}")
    public ResponseEntity<Integer> findModulo(@PathVariable("matricula") String matricula) {
        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        Alumno alumno = alumnoService.getByMatricula(matricula).get();
        Set<Modulo> modulos = alumno.getModulos();
        Integer mod = 1;
        for (Modulo m : modulos) {
            if (m.getSemestre().isActivo()) {
                mod = m.getIdModulo();
            }
        }

        return new ResponseEntity<Integer>(mod, HttpStatus.OK);

    }

    @GetMapping("/listaProfesor/{idProfesor}")
    public ResponseEntity<List<Object[]>> listByProfesor(@PathVariable("idProfesor") int idProfesor) {
        List<Object[]> list = alumnoService.listAllByIDProfesor(idProfesor);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
