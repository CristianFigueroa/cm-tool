package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.dto.ProfesorDto;
import cl.ufro.dci.cmtool.enums.NombreRol;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.entity.Rol;
import cl.ufro.dci.cmtool.model.service.ProfesorService;
import cl.ufro.dci.cmtool.model.service.RolService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping("/profesor")
public class ProfesorController extends GenericoController {

    @Autowired
    ProfesorService profesorService;

    @Autowired
    RolService rolService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/lista")
    public ResponseEntity<List<Profesor>> list() {
        List<Profesor> list = profesorService.list();
        return new ResponseEntity<List<Profesor>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{id}")
    public ResponseEntity<Profesor> getById(@PathVariable("id") int id) {
        if (!profesorService.existsById(id))
            return new ResponseEntity(new Mensaje("El docente no existe"), HttpStatus.BAD_REQUEST);
        Profesor profesor = profesorService.getById(id).get();
        return new ResponseEntity<Profesor>(profesor, HttpStatus.OK);
    }

    @PostMapping("/nuevo")
    public ResponseEntity<?> create(@Valid @RequestBody ProfesorDto profesorDto) {
        if (profesorService.existsByEmail(profesorDto.getCorreoProfesor()))
            return new ResponseEntity(new Mensaje("El docente ya esta registrado"), HttpStatus.BAD_REQUEST);

        Profesor profesor = new Profesor(profesorDto.getNombreProfesor(), profesorDto.getCorreoProfesor(),
                passwordEncoder.encode(profesorDto.getContraseniaProfesor()), profesorDto.getContraseniaProfesor());

        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolNombre(NombreRol.ROLE_PROFESOR).get());
        // con esto comentado siempre sera administrador
        // if (profesorDto.getRoles().contains("administrador") ||
        // profesorDto.getRoles().contains("ADMINISTRADOR"))
        roles.add(rolService.getByRolNombre(NombreRol.ROLE_ADMINISTRADOR).get());
        profesor.setRolProfesor(roles);
        profesorService.save(profesor);

        return new ResponseEntity(new Mensaje("Docente creado correctamente"), HttpStatus.CREATED);
    }

    @PutMapping("/editar/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id,
            @RequestBody ProfesorDto profesorDto) {
        if (!profesorService.existsById(id))
            return new ResponseEntity(new Mensaje("El docente no existe"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(profesorDto.getNombreProfesor()))
            return new ResponseEntity(new Mensaje("El nombre no puede estar en blanco"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(profesorDto.getContraseniaProfesor()))
            return new ResponseEntity(new Mensaje("El password no puede estar en blanco"), HttpStatus.BAD_REQUEST);
        if (profesorService.existsByEmail(profesorDto.getCorreoProfesor())
                && profesorService.getByCorreo(profesorDto.getCorreoProfesor()).get().getIdProfesor() != id)
            return new ResponseEntity(new Mensaje("El correo ya esta en uso"), HttpStatus.BAD_REQUEST);

        Profesor profesor = profesorService.getById(id).get();
        profesor.setNombreProfesor(profesorDto.getNombreProfesor());
        profesor.setCorreoProfesor(profesorDto.getCorreoProfesor());
        profesor.setContraseniaProfesor(profesorDto.getContraseniaProfesor());

        profesorService.save(profesor);
        return new ResponseEntity(new Mensaje("Docente editado correctamente"), HttpStatus.OK);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") int id) {
        if (!profesorService.existsById(id))
            return new ResponseEntity(new Mensaje("El docente no existe"), HttpStatus.BAD_REQUEST);
        profesorService.delete(id);
        return new ResponseEntity(new Mensaje("Docente eliminado correctamente"), HttpStatus.OK);
    }
}
