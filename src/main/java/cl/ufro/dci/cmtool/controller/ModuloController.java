package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.configuration.JwtProvider;
import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.dto.ModuloDto;
import cl.ufro.dci.cmtool.model.entity.Modulo;
import cl.ufro.dci.cmtool.model.service.ModuloService;
import cl.ufro.dci.cmtool.model.service.ProfesorService;
import cl.ufro.dci.cmtool.model.service.SemestreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping("/modulo")
public class ModuloController extends GenericoController {

    @Autowired
    ModuloService moduloService;

    @Autowired
    ProfesorService profesorService;

    @Autowired
    SemestreService semestreService;

    @Autowired
    JwtProvider jwtProvider;

    @GetMapping("/lista")
    public ResponseEntity<Set<Modulo>> list() {
        Set<Modulo> list = moduloService.listAllActivo();
        return new ResponseEntity<Set<Modulo>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{codigoModulo}")
    public ResponseEntity<Modulo> getByCodigoModulo(@PathVariable("codigoModulo") String codigoModulo) {
        if (!moduloService.existByCodigoModulo(codigoModulo))
            return new ResponseEntity(new Mensaje("Este módulo no existe"), HttpStatus.BAD_REQUEST);
        Modulo modulo = moduloService.getByCodigoModulo(codigoModulo).get();
        return new ResponseEntity<Modulo>(modulo, HttpStatus.OK);
    }

    @GetMapping("/detalle")
    public ResponseEntity<Modulo> getByProfesor(@RequestHeader (name="Authorization") String token) {
        String correoProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        if(!moduloService.existActiveByEmailProfesor(correoProfesor))
            return new ResponseEntity(new Mensaje("El docente no cuenta con un módulo en el semestre actual"), HttpStatus.BAD_REQUEST);
        Modulo modulo = moduloService.getActiveByCorreoProfesor(correoProfesor).get();
        return new ResponseEntity<Modulo>(modulo, HttpStatus.OK);
    }

    @PostMapping("/nuevo/{idProfesor}")
    public ResponseEntity<?> create(@Valid @RequestBody ModuloDto moduloDto, @PathVariable("idProfesor") int idProfesor, BindingResult result){
        if(!semestreService.existActivo())
            return new ResponseEntity(new Mensaje("No existe un semestre activo"),HttpStatus.BAD_REQUEST);
        if(moduloService.existByCodigoModulo(moduloDto.getCodigoModulo()))
            return new ResponseEntity(new Mensaje("El módulo ya existe"),HttpStatus.BAD_REQUEST);
        if(!profesorService.existsById(idProfesor)){
            return new ResponseEntity(new Mensaje("El docente no existe"),HttpStatus.BAD_REQUEST);
        }else{
            for(Modulo modulo: profesorService.getById(idProfesor).get().getModulos()){
                if(modulo.getSemestre().isActivo()){
                    return new ResponseEntity(new Mensaje("El docente ya esta registrado en un módulo este semestre"),HttpStatus.BAD_REQUEST);
                }
            }
        }
        Modulo modulo = new Modulo(moduloDto.getCodigoModulo(), semestreService.getActivo().get(), profesorService.getById(idProfesor).get());
        moduloService.save(modulo);
        return new ResponseEntity(new Mensaje("Módulo creado correctamente"),HttpStatus.CREATED);
    }


}
