package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.dto.SemestreDto;
import cl.ufro.dci.cmtool.model.entity.Semestre;
import cl.ufro.dci.cmtool.model.service.SemestreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping("/semestre")
public class SemestreController extends GenericoController {

    @Autowired
    SemestreService semestreService;

    @GetMapping("/lista")
    public ResponseEntity<Set<Semestre>> list() {
        Set<Semestre> list = semestreService.listAll();
        return new ResponseEntity<Set<Semestre>>(list, HttpStatus.OK);
    }

    @GetMapping("/activo")
    public ResponseEntity<Semestre> getActivo() {
        if (!semestreService.existActivo())
            return new ResponseEntity(new Mensaje("No hay semestres creados"), HttpStatus.BAD_REQUEST);
        Semestre semestre = semestreService.getActivo().get();
        return new ResponseEntity<Semestre>(semestre, HttpStatus.OK);
    }

    @PostMapping("/nuevo")
    public ResponseEntity<?> create(@Valid @RequestBody SemestreDto semestreDto, BindingResult result){
        if(semestreService.existByAnioSemestreAndNumeroSemestre(semestreDto.getAnioSemestre(),semestreDto.getNumeroSemestre()))
            return new ResponseEntity<>(new Mensaje("El semestre ya existe"),HttpStatus.BAD_REQUEST);
        if(semestreService.existActivo()) {
            Semestre semestreAnterior = semestreService.getActivo().get();
            semestreAnterior.setActivo(false);
            semestreService.save(semestreAnterior);
        }
        Semestre semestre = new Semestre(semestreDto.getAnioSemestre(), semestreDto.getNumeroSemestre());
        semestreService.save(semestre);
        return new ResponseEntity(new Mensaje("Semestre creado correctamente"),HttpStatus.CREATED);
    }

    @PutMapping("/cambiarActivo/{idSemestre}")
    public ResponseEntity<?> cambiarActivo(@PathVariable("idSemestre") int idSemestre){
        if (!semestreService.existByIdSemestre(idSemestre))
            return new ResponseEntity<>(new Mensaje("Semestre no encontrado"), HttpStatus.BAD_REQUEST);
        if(semestreService.existActivo()){
            Semestre semestreAntiguo = semestreService.getActivo().get();
            semestreAntiguo.setActivo(false);
            semestreService.save(semestreAntiguo);
        }
        Semestre semestre = semestreService.getByIdSemestre(idSemestre).get();
        semestre.setActivo(true);
        semestreService.save(semestre);
        return new ResponseEntity<>(new Mensaje("Semestre actualizado correctamente"), HttpStatus.OK);
    }
}
