package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.model.entity.Categoria;
import cl.ufro.dci.cmtool.model.entity.TipoIndicador;
import cl.ufro.dci.cmtool.model.service.CategoriaService;
import cl.ufro.dci.cmtool.model.service.TipoIndicadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/plantilla")
public class PlantillaController extends GenericoController {

    @Autowired
    TipoIndicadorService tipoIndicadorService;

    @Autowired
    CategoriaService categoriaService;

    @GetMapping("/listar/{nivel}")
    public ResponseEntity<Set<TipoIndicador>> listaIndicadoresByNivel(@PathVariable("nivel") int nivel) {
        switch (nivel) {
            case 1:
                nivel = 17;
                break;
            case 2:
                nivel = 18;
                break;
            case 3:
                nivel = 19;
                break;
            case 4:
                nivel = 20;
                break;
            case 5:
                nivel = 21;
                break;
            case 6:
                nivel = 22;
                break;
            default:
                nivel = 99;
        }
        Set<TipoIndicador> list = tipoIndicadorService.getByNivel(nivel);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/listarByIdCategoria/{categoria}")
    public ResponseEntity<Set<TipoIndicador>> listaIndicadoresByCategoria(@PathVariable("categoria") int idCategoria) {
        Set<TipoIndicador> list = tipoIndicadorService.getByIdCategoria(idCategoria);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/listar/categoria")
    public ResponseEntity<List<Categoria>> listaCategoria() {
        List<Categoria> list = categoriaService.listAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/listar/nombresIndicadores")
    public ResponseEntity<Set<String>> listarNombresIndicadores() {
        Set<String> list = tipoIndicadorService.getDistinctIndicatorNames();
        return new ResponseEntity<Set<String>>(list, HttpStatus.OK);

    }
}
