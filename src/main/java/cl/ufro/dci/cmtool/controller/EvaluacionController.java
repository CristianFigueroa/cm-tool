package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.configuration.JwtProvider;
import cl.ufro.dci.cmtool.dto.EvaluacionDto;
import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Evaluacion;
import cl.ufro.dci.cmtool.model.entity.Modulo;
import cl.ufro.dci.cmtool.model.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping("/evaluacion")
public class EvaluacionController extends GenericoController {

    @Autowired
    RubricaService rubricaService;

    @Autowired
    IndicadorService indicadorService;

    @Autowired
    EvaluacionService evaluacionService;

    @Autowired
    AlumnoService alumnoService;

    @Autowired
    ProfesorService profesorService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    ModuloService moduloService;

    @GetMapping("/listaAdmin")
    public ResponseEntity<List<Evaluacion>> listAdmin() {
        List<Evaluacion> list = evaluacionService.listAll();
        return new ResponseEntity<List<Evaluacion>>(list, HttpStatus.OK);
    }

    @GetMapping("/lista")
    public ResponseEntity<Set<Evaluacion>> list(@RequestHeader(name = "Authorization") String token) {
        String emailProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        Set<Evaluacion> list = evaluacionService.listAllByProfesor(emailProfesor);
        return new ResponseEntity<Set<Evaluacion>>(list, HttpStatus.OK);
    }

    @GetMapping("/lista/{matricula}")
    public ResponseEntity<Set<Evaluacion>> listaEvs(@PathVariable("matricula") String matricula) {

        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        Alumno alumno = alumnoService.getByMatricula(matricula).get();
        Set<Modulo> modulos = alumno.getModulos();
        String emailProfesor = "";
        for (Modulo m : modulos) {
            if (m.getSemestre().isActivo()) {
                emailProfesor = m.getProfesor().getCorreoProfesor();
            }
        }

        Set<Evaluacion> list = evaluacionService.listAllByProfesor(emailProfesor);

        return new ResponseEntity<Set<Evaluacion>>(list, HttpStatus.OK);
    }

    @GetMapping("/detalle/{idEvaluacion}")
    public ResponseEntity<Evaluacion> getById(@PathVariable("idEvaluacion") int idEvaluacion) {
        if (!evaluacionService.existById(idEvaluacion))
            return new ResponseEntity(new Mensaje("La evaluacion no existe"), HttpStatus.BAD_REQUEST);
        Evaluacion evaluacion = evaluacionService.getById(idEvaluacion).get();
        return new ResponseEntity<Evaluacion>(evaluacion, HttpStatus.OK);
    }

    @PutMapping("/actualizar/{idEvaluacion}")
    public ResponseEntity<?> update(@PathVariable("idEvaluacion") int idEvaluacion,
            @Valid @RequestBody EvaluacionDto evaluacionDto) {
        if (!evaluacionService.existById(idEvaluacion))
            return new ResponseEntity(new Mensaje("La evaluacion no existe"), HttpStatus.BAD_REQUEST);
        if (evaluacionDto.getFechaEvaluacion() == null)
            return new ResponseEntity(new Mensaje("La fecha no puede estar en blanco"), HttpStatus.BAD_REQUEST);

        Evaluacion evaluacion = evaluacionService.getById(idEvaluacion).get();
        evaluacion.setFechaEvaluacion(evaluacionDto.getFechaEvaluacion());
        evaluacion.setDescripcion(evaluacionDto.getDescripcion());
        evaluacionService.save(evaluacion);
        return new ResponseEntity(new Mensaje("Evaluacion editada correctamente"), HttpStatus.OK);
    }

    @DeleteMapping("/eliminar/{idEval}")
    public ResponseEntity<?> deleteById(@PathVariable("idEval") int idEval) {
        if (!evaluacionService.existById(idEval))
            return new ResponseEntity(new Mensaje("La evaluacion no existe"), HttpStatus.BAD_REQUEST);
        evaluacionService.delete(idEval);
        return new ResponseEntity(new Mensaje("Evaluacion eliminada correctamente"), HttpStatus.OK);
    }

    @PostMapping("/nuevo")
    public ResponseEntity<?> create(@RequestHeader(name = "Authorization") String token,
            @Valid @RequestBody EvaluacionDto evaluacionDto) {
        String emailProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        if (!moduloService.existActiveByEmailProfesor(emailProfesor))
            return new ResponseEntity(new Mensaje("El docente no esta vinculado a un módulo este semestre"),
                    HttpStatus.BAD_REQUEST);
        Modulo modulo = moduloService.getActiveByCorreoProfesor(emailProfesor).get();
        Evaluacion evaluacion = new Evaluacion(
                evaluacionDto.getFechaEvaluacion(),
                (evaluacionService.listAllByProfesor(emailProfesor).size() + 1),
                evaluacionDto.getDescripcion(),
                evaluacionDto.getNumeroPlantilla(),
                modulo);
        evaluacion.setProfesor(profesorService.getByCorreo(emailProfesor).get());
        evaluacionService.save(evaluacion);

        return new ResponseEntity(new Mensaje("Evaluacion creada correctamente"), HttpStatus.CREATED);
    }

    @GetMapping("/puntaje/{matricula}/{nombreIndicador}")
    public ResponseEntity<Set<Object[]>> findEvaluacionPuntajePorIndicadorMatricula(
            @PathVariable String matricula,
            @PathVariable String nombreIndicador) {
        Set<Object[]> result = evaluacionService.findEvaluacionPuntajePorIndicadorMatricula(
                nombreIndicador,
                matricula);

        if (result.isEmpty()) {
            return null;
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @GetMapping("/descripcion/{descripcion}")
    public ResponseEntity<Set<Evaluacion>> findByDescripcion(@PathVariable String descripcion) {
        Set<Evaluacion> result = evaluacionService.findByDescripcion(descripcion);

        if (result.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

}
