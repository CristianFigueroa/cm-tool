package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.configuration.JwtProvider;
import cl.ufro.dci.cmtool.dto.IndicadorDto;
import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.dto.RubricaDto;
import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Indicador;
import cl.ufro.dci.cmtool.model.entity.Modulo;
import cl.ufro.dci.cmtool.model.entity.Rubrica;
import cl.ufro.dci.cmtool.model.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Map;

@RestController
@RequestMapping("/rubrica")
public class RubricaController extends GenericoController {

    @Autowired
    RubricaService rubricaService;

    @Autowired
    IndicadorService indicadorService;

    @Autowired
    EvaluacionService evaluacionService;

    @Autowired
    AlumnoService alumnoService;

    @Autowired
    TipoIndicadorService tipoIndicadorService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    ModuloService moduloService;

    @GetMapping("/lista")
    public ResponseEntity<List<Rubrica>> lista() {
        List<Rubrica> list = rubricaService.list();
        return new ResponseEntity<List<Rubrica>>(list, HttpStatus.OK);
    }

    @PostMapping("/nuevo/{idEvaluacion}/{idAlumno}")
    public ResponseEntity<?> nueva(@RequestHeader(name = "Authorization") String token,
            @PathVariable("idEvaluacion") int idEvaluacion,
            @PathVariable("idAlumno") String idAlumno,
            @RequestBody RubricaDto rubricaDto) {
        String correoProfesor = jwtProvider.getEmailUsuarioFromToken(token.replace("Bearer", ""));
        if (!moduloService.existActiveByEmailProfesor(correoProfesor))
            return new ResponseEntity(new Mensaje("El docente no esta vinculado a un módulo"), HttpStatus.BAD_REQUEST);
        if (!evaluacionService.existById(idEvaluacion))
            return new ResponseEntity(new Mensaje("La evaluación no existe"), HttpStatus.BAD_REQUEST);
        if (!alumnoService.existByMatricula(idAlumno))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        if (!rubricaService.getByEvalAndAlumno(idEvaluacion, idAlumno).isEmpty())
            return new ResponseEntity(
                    new Mensaje("El estudiante ya tiene una corrección anterior para esta evaluación"),
                    HttpStatus.BAD_REQUEST);

        agregarAlumnoAEvaluacion(idEvaluacion, idAlumno);

        Rubrica rubrica = new Rubrica();
        rubrica.setAlumno(alumnoService.getByMatricula(idAlumno).get());
        rubrica.setEvaluacion(evaluacionService.getById(idEvaluacion).get());
        Modulo modulo = moduloService.getActiveByCorreoProfesor(correoProfesor).get();
        rubrica.setModulo(modulo);
        rubricaService.save(rubrica);

        Set<Indicador> indicadores = new HashSet<>();
        for (IndicadorDto indicadorDto : rubricaDto.getIndicadores()) {
            Indicador indicador = new Indicador();
            indicador.setPuntajeIndicador(indicadorDto.getPuntajeIndicador());
            indicador.setTipoIndicador(tipoIndicadorService.getById(indicadorDto.getIdTipoIndicador()).get());
            indicador.setRubrica(rubrica);
            indicadores.add(indicador);
        }
        indicadorService.saveAll(indicadores);
        return new ResponseEntity(new Mensaje("Rubrica creada correctamente"), HttpStatus.CREATED);
    }

    @PutMapping("/editar/{idEvaluacion}/{idAlumno}")
    public ResponseEntity<?> editar(@PathVariable("idEvaluacion") int idEvaluacion,
            @PathVariable("idAlumno") String idAlumno,
            @RequestBody RubricaDto rubricaDto) {
        if (!evaluacionService.existById(idEvaluacion))
            return new ResponseEntity(new Mensaje("La evaluacion no existe"), HttpStatus.BAD_REQUEST);
        if (!alumnoService.existByMatricula(idAlumno))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);

        Rubrica rubrica = rubricaService.getByEvalAndAlumno(idEvaluacion, idAlumno).get();

        Set<Indicador> indicadores = new HashSet<>();
        for (IndicadorDto indicadorDto : rubricaDto.getIndicadores()) {
            Indicador indicador = rubrica.getIndicadores().stream()
                    .filter(x -> x.getTipoIndicador().getIdTipoIndicador() == indicadorDto.getIdTipoIndicador())
                    .findFirst().get();
            indicador.setPuntajeIndicador(indicadorDto.getPuntajeIndicador());
            indicadores.add(indicador);
        }
        indicadorService.saveAll(indicadores);
        return new ResponseEntity(new Mensaje("Rubrica actualizada correctamente"), HttpStatus.OK);
    }

    @GetMapping("/{idEvaluacion}/{matricula}")
    public ResponseEntity<Rubrica> getRubrica(@PathVariable("idEvaluacion") int idEvaluacion,
            @PathVariable("matricula") String matricula) {
        if (!alumnoService.existByMatricula(matricula))
            return new ResponseEntity(new Mensaje("El estudiante no existe"), HttpStatus.BAD_REQUEST);
        Optional<Rubrica> rubrica = rubricaService.getByEvalAndAlumno(idEvaluacion, matricula);
        if (!evaluacionService.existById(idEvaluacion))
            return new ResponseEntity(new Mensaje("La evaluación no esta creada"), HttpStatus.BAD_REQUEST);
        if (rubrica.isEmpty())
            return new ResponseEntity(new Mensaje("La rubrica no existe"), HttpStatus.BAD_REQUEST);
        if (rubrica.get().getIndicadores().isEmpty())
            return new ResponseEntity(new Mensaje("La rubrica no tiene contenido"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Rubrica>(rubrica.get(), HttpStatus.OK);
    }

    public void agregarAlumnoAEvaluacion(int idEvaluacion, String idAlumno) {
        if (!evaluacionService.getById(idEvaluacion).get().getAlumnos()
                .contains(alumnoService.getByMatricula(idAlumno))) {
            Set<Alumno> lista = evaluacionService.getById(idEvaluacion).get().getAlumnos();
            lista.add(alumnoService.getByMatricula(idAlumno).get());
            evaluacionService.getById(idEvaluacion).get().setAlumnos(lista);
        }
    }

    // controlador que retorna lista de strings de RA
    @GetMapping("/getra")
    public List<String> obtenerRA() {
        return rubricaService.obtenerRA();
    }

    // controlador que retorna lista de strings de RA
    @GetMapping("/getindicador")
    public List<String> obtenerIndicador() {
        return rubricaService.obtenerIndicador();
    }

    @GetMapping("/getraeindicadores")
    public List<Map<String, Object>> obtenerRAeIndicadores() {
        return rubricaService.obtenerRAConIndicadores();
    }
}
