package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.dto.Mensaje;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;

public class GenericoController {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Mensaje> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        return new ResponseEntity<>(new Mensaje(ex.getMessage().split(": ")[1]), HttpStatus.BAD_REQUEST);
    }
}
