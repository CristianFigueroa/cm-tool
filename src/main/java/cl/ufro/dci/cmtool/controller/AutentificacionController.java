package cl.ufro.dci.cmtool.controller;

import cl.ufro.dci.cmtool.configuration.JwtProvider;
import cl.ufro.dci.cmtool.dto.JwtDto;
import cl.ufro.dci.cmtool.dto.LoginProfesorDto;
import cl.ufro.dci.cmtool.dto.Mensaje;
import cl.ufro.dci.cmtool.model.entity.Alumno;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.service.AlumnoService;
import cl.ufro.dci.cmtool.model.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

@RestController
@RequestMapping("/autentificacion")
public class AutentificacionController extends GenericoController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    RolService rolService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AlumnoService alumnoService;

    /**
     * Método para autentifciar a un Usuario
     * 
     * @param loginProfesor Objeto Dto que contiene los datos necesarios para la
     *                      autentificacion de un Usuario
     * @param bindingResult Para validar el objeto nuevoUsuario
     */
    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginProfesorDto loginProfesor,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("Campos incorrectos"), HttpStatus.BAD_REQUEST);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginProfesor.getEmail(), loginProfesor.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(token, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }

    @PostMapping("/loginById")
    public ResponseEntity<JwtDto> loginById(@Valid @RequestBody Map<String, Integer> body) {
        int profesorId = body.get("profesorId");
        List<Object[]> profesorData = alumnoService.listAllByIDProfesor(profesorId);
        if (profesorData.isEmpty()) {
            return new ResponseEntity(new Mensaje("Profesor no encontrado"), HttpStatus.NOT_FOUND);
        }
        Object[] data = profesorData.get(0);
        String email = (String) data[3]; // Correo electrónico en la posición 3
        String password = (String) data[2]; // Contraseña en la posición 2

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(token, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }

}
