package cl.ufro.dci.cmtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmToolApplication.class, args);

	}

}
