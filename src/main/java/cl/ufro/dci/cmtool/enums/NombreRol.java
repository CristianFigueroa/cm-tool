package cl.ufro.dci.cmtool.enums;

public enum NombreRol {

    ROLE_PROFESOR,
    ROLE_ADMINISTRADOR
}
