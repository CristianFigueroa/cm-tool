package cl.ufro.dci.cmtool.configuration;

import cl.ufro.dci.cmtool.enums.NombreRol;
import cl.ufro.dci.cmtool.model.entity.Categoria;
import cl.ufro.dci.cmtool.model.entity.Profesor;
import cl.ufro.dci.cmtool.model.entity.Rol;
import cl.ufro.dci.cmtool.model.entity.TipoIndicador;
import cl.ufro.dci.cmtool.model.service.CategoriaService;
import cl.ufro.dci.cmtool.model.service.ProfesorService;
import cl.ufro.dci.cmtool.model.service.RolService;
import cl.ufro.dci.cmtool.model.service.TipoIndicadorService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Map;

/**
 * Clase de configuracion que llena la tabla Rol cuando esta se encuentra vacia
 * 
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
@Component
public class Precarga implements ApplicationRunner {

    @Autowired
    RolService rolService;

    @Autowired
    ProfesorService profesorService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CategoriaService categoriaService;

    @Autowired
    TipoIndicadorService tipoIndicadorService;

    public void precargaRoles() {
        try {
            rolService.getByRolNombre(NombreRol.ROLE_PROFESOR).get();
        } catch (NoSuchElementException e) {
            rolService.save(new Rol(NombreRol.ROLE_PROFESOR));
            rolService.save(new Rol(NombreRol.ROLE_ADMINISTRADOR));
        }
    }

    public void precargaAdmin() {
        if (!profesorService.existsByEmail("admin@ufrontera.cl")) {
            Rol rol;
            Profesor user;
            String password = "pass";
            if (rolService.getByRolNombre(NombreRol.ROLE_ADMINISTRADOR).isPresent()) {
                rol = rolService.getByRolNombre(NombreRol.ROLE_ADMINISTRADOR).get();
                user = new Profesor("admin", "admin@ufrontera.cl", passwordEncoder.encode(password), rol, password);
            } else {
                user = new Profesor("admin", "admin@ufrontera.cl", passwordEncoder.encode(password), password);
            }
            profesorService.save(user);
        }
    }

    public void precargaRubrica() {
        ClassPathResource cpr = new ClassPathResource("files/Rubrica.xlsx");
        try {
            XSSFWorkbook wb = new XSSFWorkbook(cpr.getInputStream());
            XSSFSheet sheetRubrica = wb.getSheetAt(0); // hoja rubrica

            Iterator<Row> rowIterator = sheetRubrica.iterator();
            rowIterator.next();
            int count = 1;
            String nombreCategoriaActual = "";
            while (rowIterator.hasNext()) {
                Row nextRow = rowIterator.next();
                if (nextRow.getCell(1).toString().contains(count + ".")) {
                    nombreCategoriaActual = nextRow.getCell(1).toString().replace(' ', ' ');
                    if (!categoriaService.existByNombre(nombreCategoriaActual)) {
                        Categoria categoria = new Categoria(nombreCategoriaActual);
                        categoriaService.save(categoria);
                    }
                    count++;
                }
                if (!nombreCategoriaActual.equals("") && nextRow.getCell(2) != null
                        && !nextRow.getCell(2).toString().isEmpty()) {
                    TipoIndicador tipoIndicador = new TipoIndicador(nextRow.getCell(2).toString(),
                            categoriaService.getByNombreCategoria(nombreCategoriaActual).get());
                    tipoIndicador.setNivelIndicador(1);
                    tipoIndicador.setCriterio0(nextRow.getCell(3).toString());
                    tipoIndicador.setCriterio1(nextRow.getCell(4).toString());
                    tipoIndicador.setCriterio2(nextRow.getCell(5).toString());
                    tipoIndicador.setCriterio3(nextRow.getCell(6).toString());
                    tipoIndicador.setCriterio4(nextRow.getCell(7).toString());
                    if (tipoIndicadorService.existByNombre(nextRow.getCell(2).toString())) {
                        tipoIndicador.setIdTipoIndicador(tipoIndicadorService.getByNombre(nextRow.getCell(2).toString())
                                .get().getIdTipoIndicador());
                    }
                    tipoIndicadorService.save(tipoIndicador);
                }
            }
            wb.close();
            precargaSemanas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void precargaSemanas() {
        ClassPathResource cpr = new ClassPathResource("files/Rubrica.xlsx");
        try {
            XSSFWorkbook wb = new XSSFWorkbook(cpr.getInputStream());
            XSSFSheet sheetSemana = wb.getSheetAt(1); // hoja rubrica

            Iterator<Row> rowIterator = sheetSemana.iterator();
            rowIterator.next();

            while (rowIterator.hasNext()) {
                Row nextRow = rowIterator.next();
                if (tipoIndicadorService.existByNombre(String.valueOf(nextRow.getCell(1)))) {
                    TipoIndicador indicador = tipoIndicadorService.getByNombre(String.valueOf(nextRow.getCell(1)))
                            .get();
                    List<Integer> semanas = new ArrayList<>();
                    for (int i = 1; i <= 22; i++) {
                        if (String.valueOf(nextRow.getCell(i + 1)).equals("X"))
                            semanas.add(i);
                    }
                    indicador.setSemanas(semanas);
                    tipoIndicadorService.save(indicador);
                }
            }
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        precargaRoles();
        precargaAdmin();
        precargaRubrica();
    }

    // funcion que elimina numeros y espacios de una lista de strings
    public static String eliminarNumerosYEspacios(String input) {
        int indicePrimerLetraVerbo = -1;
        for (int j = 0; j < input.length(); j++) {
            char caracterActual = input.charAt(j);
            if (Character.isLetter(caracterActual)) {
                indicePrimerLetraVerbo = j;
                break;
            }
        }
        if (indicePrimerLetraVerbo != -1) {
            return input.substring(indicePrimerLetraVerbo);
        }
        return input;
    }

    // funcion que elimina numeros y espacios de una lista de strings
    public static List<String> eliminarNumerosYEspacios(List<String> strings) {
        List<String> stringsActualizados = new ArrayList<>();
        for (String string : strings) {
            String stringActualizada = eliminarNumerosYEspacios(string);
            stringsActualizados.add(stringActualizada);
        }
        return stringsActualizados;
    }

    public List<Map<String, Object>> obtenerRAConIndicadores() {
        List<Map<String, Object>> listaRA = new ArrayList<>();
        ClassPathResource cpr = new ClassPathResource("files/Rubrica.xlsx");
        try {
            XSSFWorkbook wb = new XSSFWorkbook(cpr.getInputStream());
            XSSFSheet sheetRubrica = wb.getSheetAt(0); // hoja rubrica
            Iterator<Row> rowIterator = sheetRubrica.iterator();
            rowIterator.next();
            int count = 1;
            String nombreCategoriaActual = "";
            Map<String, Object> raActual = null;
            while (rowIterator.hasNext()) {
                Row nextRow = rowIterator.next();
                if (nextRow.getCell(1).toString().contains(count + ".")) {
                    nombreCategoriaActual = nextRow.getCell(1).toString().replace(' ', ' ');
                    raActual = new HashMap<>();
                    raActual.put("ra", eliminarNumerosYEspacios(nombreCategoriaActual));
                    raActual.put("indicadores", new ArrayList<String>());
                    listaRA.add(raActual);
                    count++;
                }
                if (raActual != null && nextRow.getCell(2) != null
                        && !nextRow.getCell(2).toString().isEmpty()) {
                    String indicador = nextRow.getCell(2).toString();
                    List<String> indicadores = (List<String>) raActual.get("indicadores");
                    indicadores.add(eliminarNumerosYEspacios(indicador));
                }
            }
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaRA;
    }

    // funcion que retorna lista de strings de RA
    public List<String> obtenerRA() {
        List<String> listaRA = new ArrayList<>();
        ClassPathResource cpr = new ClassPathResource("files/Rubrica.xlsx");
        try {
            XSSFWorkbook wb = new XSSFWorkbook(cpr.getInputStream());
            XSSFSheet sheetRubrica = wb.getSheetAt(0); // hoja rubrica
            Iterator<Row> rowIterator = sheetRubrica.iterator();
            rowIterator.next();
            int count = 1;
            String nombreCategoriaActual = "";
            while (rowIterator.hasNext()) {
                Row nextRow = rowIterator.next();
                if (nextRow.getCell(1).toString().contains(count + ".")) {
                    nombreCategoriaActual = nextRow.getCell(1).toString().replace(' ', ' ');
                    listaRA.add((nombreCategoriaActual));
                    count++;
                }
            }
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return eliminarNumerosYEspacios(listaRA);
    }

    // funcion que retorna lista de strings de indicadores
    public List<String> obtenerIndicador() {

        List<String> listaIndicadores = new ArrayList<>();
        ClassPathResource cpr = new ClassPathResource("files/Rubrica.xlsx");
        try {
            XSSFWorkbook wb = new XSSFWorkbook(cpr.getInputStream());
            XSSFSheet sheetRubrica = wb.getSheetAt(0); // hoja rubrica
            Iterator<Row> rowIterator = sheetRubrica.iterator();
            rowIterator.next();
            int count = 1;
            String nombreCategoriaActual = "";
            while (rowIterator.hasNext()) {
                Row nextRow = rowIterator.next();
                if (nextRow.getCell(1).toString().contains(count + ".")) {
                    nombreCategoriaActual = nextRow.getCell(1).toString().replace(' ', ' ');
                    // listaRA.add((nombreCategoriaActual));
                    // count++;
                    if (!categoriaService.existByNombre(nombreCategoriaActual)) {
                        Categoria categoria = new Categoria(nombreCategoriaActual);
                        categoriaService.save(categoria);
                    }
                    count++;
                }
                if (!nombreCategoriaActual.equals("") && nextRow.getCell(2) != null
                        && !nextRow.getCell(2).toString().isEmpty()) {
                    listaIndicadores.add(nextRow.getCell(2).toString());

                }
            }
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return eliminarNumerosYEspacios(listaIndicadores);
    }

}