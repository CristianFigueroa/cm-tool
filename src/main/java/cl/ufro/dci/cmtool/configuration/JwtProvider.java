package cl.ufro.dci.cmtool.configuration;

import cl.ufro.dci.cmtool.dto.ProfesorDto;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Clase genera token y contiene metodos de validacion para permitir ver si esta bien generado y/o expxirado
 * Esta clase contiene los atributos necesarios para la autentificacion
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
@Component
public class JwtProvider {

    private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;

    /**
     * Método que genera y retorna un token
     * @param authentication Clase generica de autentificacion que contiene los datos del usuario autenticado
     * @return Token generado
     */
    public String generateToken(Authentication authentication){
        ProfesorDto profesorDto = (ProfesorDto) authentication.getPrincipal();
        return Jwts.builder().setSubject(profesorDto.getUsername())
                .setIssuedAt(new Date()) // fecha de creacion
                //.setExpiration(new Date(new Date().getTime() + expiration)) // fecha de expiracion
                .signWith(SignatureAlgorithm.HS512, secret) // tipo de algoritmo empleado en la creacion
                .compact();
    }

    /**
     * Método que retorna el "username" del profesor, en este caso el email.
     * @param token Entregado en la peticion de acceso al recurso.
     * @return El email del profesor.
     */
    public String getEmailUsuarioFromToken(String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Método que valida un token
     * @param token Entregado en la peticion de acceso al recurso.
     * @return booleano
     */
    public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }catch (MalformedJwtException e){
            logger.error("Token mal formado");
        }catch (UnsupportedJwtException e){
            logger.error("Token no soportado");
        }catch (ExpiredJwtException e){
            logger.error("Token expirado");
        }catch (IllegalArgumentException e){
            logger.error("Token vacio");
        }catch (SignatureException e){
            logger.error("Error en la firma");
        }
        return false;
    }
}
