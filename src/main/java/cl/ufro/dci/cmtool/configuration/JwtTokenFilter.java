package cl.ufro.dci.cmtool.configuration;

import cl.ufro.dci.cmtool.model.service.ProfesorDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Clase que se llama para cada peticion, comprueba mediante provider la validez del token, si es valido permite acceso al recurso
 * Esta clase contiene los atributos necesarios para la autentificacion
 * @author: Cristian Figueroa
 * @version: 01/09/2020
 */
public class JwtTokenFilter extends OncePerRequestFilter {

    private final static Logger logger = LoggerFactory.getLogger(JwtTokenFilter.class);

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    ProfesorDetailService profesorDetailService;

    /**
     * Metodo que verifica el token, se extrae la información del mismo para establecer la identidad del profesor dentro del contexto de seguridad de la aplicación
     * @param request Token recibido.
     * @param response
     * @param filterChain
     * @throws ServletException Error para una instancia de servlet
     * @throws IOException Error en la lectura del token.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = getToken(request);
            if (token != null && jwtProvider.validateToken(token)) {
                String emailUsuario = jwtProvider.getEmailUsuarioFromToken(token);
                UserDetails userDetails = profesorDetailService.loadUserByUsername(emailUsuario);
                UsernamePasswordAuthenticationToken auth =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        } catch (Exception e) {
            logger.error("Error en metodo doFilter");
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Método que elimina el "Bearer" del token
     * @param request Token recibido.
     * @return El token sin el segmento "Bearer "
     */
    private String getToken(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer"))
            return header.replace("Bearer", "");
        return null;
    }
}
