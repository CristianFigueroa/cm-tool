package cl.ufro.dci.cmtool.helper;

import cl.ufro.dci.cmtool.model.entity.Alumno;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jxl.Workbook;
import jxl.Sheet;
import jxl.Cell;

public class GestorExcelAlumnos {

    public static List<Alumno> read(String path) {
        List<Alumno> alumnos = new ArrayList<>();
        try {
            Workbook wb = Workbook.getWorkbook(new File(path));
            Sheet sheet = wb.getSheet(0);

            for (int i = 7; i < sheet.getRows(); i++) {
                Alumno alumno = new Alumno();
                Cell cell = sheet.getCell(3, i);
                // Ignorar las filas que no contienen una matrícula válida
                if (cell.getContents().isEmpty()
                        || (!cell.getContents().contains("K") && !cell.getContents().matches("-?\\d+(\\.\\d+)?"))) {
                    continue;
                }
                if (cell.getContents().contains("K")) {
                    alumno.setMatriculaAlumno(cell.getContents());
                } else {
                    // Comprobar si el contenido de la celda es un número
                    if (cell.getContents().matches("-?\\d+(\\.\\d+)?")) {
                        alumno.setMatriculaAlumno((new BigDecimal(Double.valueOf(cell.getContents()))).toString());
                    } else {
                        alumno.setMatriculaAlumno(cell.getContents());
                    }
                }
                alumno.setNombreAlumno(sheet.getCell(2, i).getContents());
                alumno.setCodigoCarrera(1234); // Código de carrera fijo
                alumnos.add(alumno);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alumnos;
    }

}
