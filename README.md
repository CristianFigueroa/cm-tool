# CM-TOOL Backend

Este proyecto es el backend de CM-TOOL, desarrollado con Spring Boot.

## Requisitos

- Java 11+
- MySQL 8.0.36+

## Configuración y ejecución local

1. Clona el proyecto:

```bash
git clone git@gitlab.com:CristianFigueroa/cm-tool.git
```

2. Inicia Git Flow:

```bash
git flow init
```

3. Cambia a la rama `master` con el siguiente comando:

```bash
git switch master
```

4. En `application.properties`, cambia el perfil a `dev`.

5. Cambia tus credenciales de MySQL en `application-dev.properties`.

6. Crea la base de datos `cmtoolxx` en MySQL.

6. Ejecuta el proyecto `CmToolApplication.java`.

La aplicación se ejecutará en `localhost:8080`.

## Despliegue en servidor (VPS)

1. **Configura las variables del VPS**: Ve a [Configuración de CI/CD](https://gitlab.com/CristianFigueroa/cm-tool/-/settings/ci_cd) y configura las variables. Para las variables relacionadas con Docker, utiliza las credenciales de [Docker Hub](https://hub.docker.com/) (puedes usar las tuyas o mantener las existentes). Para las variables del servidor, utiliza las credenciales de tu VPS y la clave SSH correspondiente (esto incluye crearla y moverla a autorizadas en la VPS).

2. Instala Docker y Docker Compose: Necesitas tener Docker y Docker Compose instalados en tu servidor VPS.

3. Otorga permisos a tu usuario de Docker: Ejecuta el siguiente comando para agregar tu usuario al grupo Docker, lo que te permitirá ejecutar comandos de Docker sin necesidad de `sudo`:

```bash
sudo usermod -aG docker usuario
```

4. Inicia el pipeline: Ve a [Pipelines](https://gitlab.com/CristianFigueroa/cm-tool/-/pipelines) en tu proyecto de GitLab y comienza el pipeline.

5. Accede a la aplicación: Una vez que el pipeline se haya completado con éxito, podrás acceder a la aplicación en `ipservidor:8080`.

## Apartado EMRA

Para que funcione el apartado "EMRA", hay que realizar una petición con un programa de asignatura (cualquiera descargado desde intranet) como un Multipart Form llamando "file" con el archivo.

1. Realiza un POST hacia `http://ipserveremra:8080/uploadFileRA` hasta que responda con "137".
2. Luego, de la misma forma, realiza un POST hacia `http://ipserveremra:8080/uploadFileIndicador`. Esta petición debería devolver "138" con la primera petición.